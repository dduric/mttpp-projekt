# MTTPP - Projekt

Projekt iz kolegija Metode i tehnike testiranja programske podrške

# Automatsko testiranje Backenda Web aplikacija (REST API)

Testirana je jednostavna web aplikacija koja ima svoju bazu podataka.

# Upute za pokretanje aplikacije i testova

Testovi su pisani u Java programskom jeziku.
Korišteni framework je TestNG

1. Klonirati repozitorij s linka https://gitlab.com/dduric/mttpp-projekt
2. Pomoću InteliJ pokrenuti testove (**Run** -> **Test1**)
3. Unutar kloniranog repozitorija nalaze se db.json i routes.json koje je potrebno pokrenuti
4. Najjednostavniji način za pokretanje servera je pomoću Command Prompt-a
5. Pritisnuti tipke **WINDOWS** + **R**, te upisati **cmd**
6. Pomoću **cd** komande doći do foldera u kojem je klonirani repozitorij
7. Unijeti komandu **json-server --port 7000 --routes routes.json --watch db.json**
